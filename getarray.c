/* This program converts the dictionary vailable at
 * https://github.com/elastic/hunspell/blob/master/dicts/sw/sw_KE.dic
 * to a format that can be used in the Hangman game
 */
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main()
{
	char word[100];
	printf("/*The dictionary is taken from sw_KE dictionary by Jason M Githeko\n");
	printf("The license terms of the dictionary are\n");
	printf("\n");
	printf("# Copyright (C) 2004 Jason M Githeko\n");
	printf("#\n");
	printf("# This library is free software; you can redistribute it and/or\n");
	printf("# modify it under the terms of the GNU Lesser General Public\n");
	printf("# License as published by the Free Software Foundation; either\n");
	printf("# version 2.1 of the License, or (at your option) any later version.\n");
	printf("#\n");
	printf("# This library is distributed in the hope that it will be useful,\n");
	printf("# but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
	printf("# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU\n");
	printf("# Lesser General Public License for more details.\n");
	printf("#\n");
	printf("# You should have received a copy of the GNU Lesser General Public\n");
	printf("# License along with this library; if not, write to the Free Software\n");
	printf("# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA\n");
	printf("\n");
	printf("*/\n");
	printf("function getword(){\n");
	printf("var vocab = [");
	scanf("%s",&word);
	for(int i = 1; i<48316; i++)
	{ 
		scanf("%s",&word);
		if ((strchr(word,'\'') == NULL ) && (strchr(word,'.') == NULL))
		{
			printf("\"%s\", ",word);
		}
	}
	scanf("%s",&word);
	printf("\"%s\"",word);
	printf("];\n");
	printf("var num = Math.floor(48317*Math.random());\n");
	printf("return vocab[num];\n");
	printf("}\n");
	return 0;
}

